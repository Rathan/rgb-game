//raw code---------------------
var numSquares = 6
var colors = generateRandomColors(numSquares);

var squares = document.querySelectorAll(".square");
var pickedColor = pickColor();
var colorDisplay = document.getElementById("colorDisplay");
var messageDisplay = document.querySelector("#message");
var h1 = document.querySelector("h1");
var resetButton = document.querySelector("#reset");
var modeButtons = document.querySelectorAll(".mode");

for(var i = o; i < modeButtons.length; i++){
	modeButtons.addEventListener("click", function(){
		modeButtons[0].classList.remove("selected");
		modeButtons[1].classList.remove("selected");
		this.classList.add("selected");
	})
}



/*easyBtn.addEventListener("click", function(){
	hardBtn.classList.remove("selected");
	easyBtn.classList.add("selected");
	numSquares =3;
	colors = generateRandomColors(numSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	for(var i = 0; i<squares.length;i++){
		if(colors[i]){
			squares[i].style.background = colors[i];
		} else {
			squares[i].style.display = "none";
		}
	}
})
hardBtn.addEventListener("click", function(){
	easyBtn.classList.remove("selected");
	hardBtn.classList.add("selected");
	numSquares = 6;
	colors = generateRandomColors(numSquares);
	pickedColor = pickColor();
	colorDisplay.textContent = pickedColor;
	for(var i = 0; i<squares.length;i++){
			squares[i].style.background = colors[i];
			squares[i].style.display = "block";
	}
})*/

resetButton.addEventListener("click",function(){
	//generate all new colors
	colors = generateRandomColors(numSquares);
	//pick a random color from array
	pickedColor = pickColor();
	//change colorDisplay to match with the picked color
	colorDisplay.textContent = pickedColor;
	messageDisplay.textContent = "";
	this.textContent = "New Colors"
	//change colors of squares
	for(var i = 0; i < squares.length; i++){
		squares[i].style.background = colors[i];
	}
	h1.style.background = "steelblue";
})

colorDisplay.textContent = pickedColor;

for(var i = 0; i < squares.length; i++){
	// add initial colors to squares
	squares[i].style.background = colors[i];

	//add click listeners to squares
	squares[i].addEventListener("click", function() {
		//grab color of clicked squares
		var clickedColor = this.style.background;

		//compare color to pickedColor
		if(clickedColor === pickedColor) {
			messageDisplay.textContent = "Correct!!";
			resetButton.textContent = "play again?"
			changeColors(clickedColor);
			h1.style.background = clickedColor;
		} else {	
			this.style.background = "#232323";
			messageDisplay.textContent = "Try Again!!"
		}
	});
}
function changeColors(color){
	//loop through all squares
	for( var i = 0; i < squares.length;i++){
		//change each color to match the given color
		squares[i].style.background = color;
	}
}
function pickColor(){
	//generate some random numbers between o to length of that array and make a float point value
	var random = Math.floor(Math.random() * colors.length);
	return colors[random];
}
function generateRandomColors(num){
	//make an array
	var arr = [];
	//repeat num times
	for(var i = 0; i< num; i++){
		//get random color and push into array
		arr.push(randomColor());
	}
	//return the array
	return arr;

}
function randomColor(){
	//pick a "red color" from 0-255
	var r = Math.floor(Math.random() * 256 );
	//pick a "green color" from 0-255
	var g = Math.floor(Math.random() * 256 );
	//pick a "blue color" from 0-255
	var b = Math.floor(Math.random() * 256 );

	return "rgb(" + r + ", " + g + ", " + b + ")";
}

































